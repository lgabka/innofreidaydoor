import {Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class RestService {

  private HEADERS = new HttpHeaders();

  constructor(private http: HttpClient) { }

  public sendPhoto(photo): Observable<Response>  {
    const blob = this.dataURItoBlob(photo);
    const file = new File([blob], 'imageFileName.jpg', {type: 'image/jpeg'});

    let formData = new FormData();
    formData.append('file', file);
    return Observable.create(observer => {
      this.http.post('/api/photo', formData, {headers: this.HEADERS}).subscribe(
        res => {
          console.log("success!");
          console.log(JSON.stringify(res));
          observer.next(res);
        },
        err => {
          console.log("ERROR: " + JSON.stringify(err));
          observer.error(err);
      })
    });
  }

  private dataURItoBlob(dataURI) {
    const byteString = atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([arrayBuffer], { type: 'image/jpeg' });
    return blob;
 }
}
