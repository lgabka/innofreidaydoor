import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-rest',
  templateUrl: './rest.component.html',
  styleUrls: ['./rest.component.css']
})
export class RestComponent implements OnInit {

      @ViewChild("video")
      public video: ElementRef;

      @ViewChild("canvas")
      public canvas: ElementRef;

      public photo: any;

      public response: string;

      public constructor(private restService: RestService) {

      }

      public ngOnInit() { }

      public ngAfterViewInit() {
          if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
              navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
                  this.video.nativeElement.src = window.URL.createObjectURL(stream);
                  this.video.nativeElement.play();
              });
          }
      }

      public capture() {
          var context = this.canvas.nativeElement.getContext("2d").drawImage(this.video.nativeElement, 0, 0, 640, 480);
          this.photo = this.canvas.nativeElement.toDataURL("image/jpeg");
      }

      public send() {
        const photoByteArray = this.removeImageData(this.photo);
        this.restService.sendPhoto(photoByteArray).subscribe(() => {
          this.response = "Success";
        }, err => {
          this.response = "Server Error";
        });
      }

      private removeImageData(photo){
        return photo.split(',')[1];
      }

  }
